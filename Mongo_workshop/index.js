var mongojs = require('mongojs'); 
var app = require('express')(); 
const bodyParser = require('body-parser') 
var port = process.env.PORT || 8081; 
var databaseUrl = 'BOT'; 
var collections = ['Box']; 
var db = mongojs(databaseUrl, collections); 

 
app.use(bodyParser.json()) 
app.use(bodyParser.urlencoded({ extended: true })) 
 
app.post('/box', (req, res) => {   
    db.node.insert(req.body);   
    res.status(201).json(req.body) 
}) 
 
app.get('/box/Date/:date', function (req, res) {   
    var text =req.params.date   
    var words = text.split('');
    words.splice( 2, 0, "/",);
    words.splice( 5, 0, "/",);
    var date = words.join('')
    console.log(date);
    
    db.node.find({Date: date}, 
        function(err, docs) {          
            res.json(docs);   
        }); 
    }); 

app.get('/box/Name/:name', function (req, res) {   
    var name =req.params.name   
    db.node.find({Name: name}, 
        function(err, docs) {          
            res.json(docs);   
        }); 
    });

app.listen(port, function() {     
    console.log('Starting node.js on port ' + port); 
}); 