var mongojs = require('mongojs'); 
var app = require('express')(); 
const bodyParser = require('body-parser') 
var port = process.env.PORT || 8081; 
var databaseUrl = 'Topgun'; 
var collections = ['node']; 
var db = mongojs(databaseUrl, collections); 
 
 
 
app.get('/node', function (req, res) {   
    db.node.find(function(err, docs) {       
        res.json(docs);   
    }); 
}); 
 
app.get('/node/:nodeName', function (req, res) {   
    var nodeName =req.params.nodeName   
    db.node.find({name: nodeName}, 
        function(err, docs) {          
            res.json(docs);   
        }); 
    }); 
 
app.listen(port, function() {     
    console.log('Starting node.js on port ' + port); 
}); 